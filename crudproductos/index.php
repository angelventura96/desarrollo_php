<?php

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">

  <head>
    <meta charset="utf-8">
    <title>CRUD Productos</title>
    <script type="text/javascript" src="js/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
  </head>

  <style type="text/css">
	 th{padding: 10px; background: rgba(225,0,0,.5); color: white;}
  </style>

  <body>
    <div id="contenido_lista">
      <?php include("php/listadoProductos.php");?>
    </div>
    <!--Aqui se inserta-->
    <button type="button" onclick="formAgregar();">Agregar</button>
  </body>

</html>
