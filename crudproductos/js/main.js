function formProducto(id_producto){
  window.location = "php/editarProducto.php?id=" + id_producto;
}

function formAgregar(){
  window.location = "php/editarProducto.php";
}

function eliminarProducto(id_producto){
  //alert(id_producto);
  $.ajax({
    type:'POST',
    url:'php/eliminaProducto.php',
    data:{id : id_producto},
    cache : false,
    success: function (respuesta){
      alert(respuesta);
      location.reload();
    }
  });
}
