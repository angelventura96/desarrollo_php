<?php
  include("conexion.php");

  if(isset($_GET['id'])){
    $id = $_GET['id'];

    $consulta = "SELECT id_prod, nombre, precio, marca, observaciones
                 FROM prods
                 WHERE id_prod=$id;";

    $ejecuta = $conexion->query($consulta) or die("Error al consultar la tabla: <br>" . $conexion -> error);
    $datos = $ejecuta -> fetch_row();
  }
  else{
    $datos[0] = '';
    $datos[1] = '';
    $datos[2] = '';
    $datos[3] = '';
    $datos[4] = '';
  }
?>

<form action="guardaEdicion.php" method="POST">
  <table>
    <tr>
      <td>ID: </td>
      <td>
        <input type="number" name="id_prod" value="<?php echo $datos[0] ?>" readonly>
      </td>
      <td>NOMBRE: </td>
      <td>
        <input type="text" name="nombre" value="<?php echo $datos[1] ?>">
      </td>
      <td>PRECIO: </td>
      <td>
        <input type="number" name="precio" value="<?php echo $datos[2] ?>">
      </td>
      <td>MARCA: </td>
      <td>
        <input type="text" name="marca" value="<?php echo $datos[3] ?>">
      </td>
      <td>OBSERVACIONES: </td>
      <td>
        <textarea name="observaciones"><?php echo $datos[4]; ?></textarea>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <button type="submit">Guardar</button>
      </td>
    </tr>
  </table>
</form>
