<?php
  //incluir el archivo de conexion a la bD
  include("conexion.php"); //por que estamos a la misma altura de proyecto, misma carpeta

  //crear la consulta para listar datos
  $consulta = "SELECT
                 id_prod,
                 nombre,
                 precio,
                 marca,
                 observaciones
               FROM prods;";

  $ejecuta = $conexion->query($consulta) or die("Error al consultar la tabla: <br>" . $conexion -> error);
?>

<table id="lista_productos">
  <tr>
    <th>ID</th>
    <th>Nombre</th>
    <th>Precio</th>
    <th>Marca</th>
    <th>Observaciones</th>
    <th colspan="2">Editar</th>

  </tr>

<?php
  while($arreglo_resultados = $ejecuta -> fetch_row() ){
    echo '<tr>';
    echo '<td>'.$arreglo_resultados[0].'</td>';
    echo '<td>'.$arreglo_resultados[1].'</td>';
    echo '<td>'.$arreglo_resultados[2].'</td>';
    echo '<td>'.$arreglo_resultados[3].'</td>';
    echo '<td>'.$arreglo_resultados[4].'</td>';
    echo '<td><button type="button" onclick="formProducto('.$arreglo_resultados[0].');">Editar</button></td>';
    echo '<td><button type="button" onclick="eliminarProducto('.$arreglo_resultados[0].');">Eliminar</button></td>';
    echo '</tr>';
  }
?>
</table>
