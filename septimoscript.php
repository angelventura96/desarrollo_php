<?php
//PHP ORIENTADO A OBJETOS

//Definiendo una clase
echo '<br>DEFINIENDO UNA CLASE<br>';

class Fruta { //INICIA LA DEFINICION DE LA CLASE
  // Atributos o propiedades de la clase
  public $nombre;
  public $color;

  // metodos
  function set_nombre($nombre) {
    $this->nombre = $nombre;
  }
  function get_nombre() {
    return $this->nombre.' - Dentro de la funcion get_nombre';
  }
} //TERMINA LA DEFINICION DELA CLASE

//-------------------------------------------------------------------
//CREANDO OBJETOS
echo '<br><br>CREANDO OBJETOS<br>';
echo '<br>Para este punto la clase Fruta() ya existe, ahora crearemos dos objetos<br>';

$manzana = new Fruta(); //Instanciamos un objeto de tipo Fruta, en una variable $manzana
$banana = new Fruta(); //Instanciamos un objeto de tipo Fruta, en una variable $banana

$manzana->set_nombre('Manzana');//Accedemos al metodo set_nombre del objeto con '->'
$banana->set_nombre('Banana');

echo $manzana->get_nombre();//Accedemos al metodo get_nombre del objeto que nos regresara
                            //el valor de la propiedad nombre del objeto
echo "<br>";
echo $banana->get_nombre();

//--------------------------------------------------------------------------
//PALABRA RESERVADA $this
echo '<br><br>PALABRA RESERVADA THIS<br>';

//Podemos ver el uso de $this en las lineas 14 y 17 del script
echo 'Hace referencia al objeto actual, con $this, podemos setear el valor de un atributo';

//--------------------------------------------------------------------------------
//Verificar si el objeto es una instancia de cierta clase
echo '<br><br>INSTANCEOF<br>';

//creamos una clase diferente a fruta para comprobar
class Verdura{}

$pera = new Fruta(); //Instanciamos un nuevo objeto de fruta
$verdura = new Verdura();//Instanciamos un nuevo objeto de verdura

echo 'pera es una instancia de Fruta? : ';
var_dump($pera instanceof Fruta);
echo '<br>';
echo 'pera es una instancia de Verdura? : ';
var_dump($pera instanceof Verdura);
echo '<br>';
echo '$verdura es una instancia de Fruta? : ';
var_dump($verdura instanceof Fruta);
echo '<br>';
echo '$verdura es una instancia de Verdura? : ';
var_dump($verdura instanceof Verdura);
echo '<br>';

//------------------------------------------------------------------------------------
//METODO CONSTRUCTOR
echo '<br><br>METODO CONSTRUCTOR<br>';

//definimos una nueva clase la cual tendra metodo constructor definido
class Postre{

  public $nombre;

  function __construct($nombre){ //el metodo constructor recibe un paremetro $nombre
    $this->nombre = $nombre; //con this, asignamos ese parametro a la propiedad nombre
                            //de ESE OBJETO , el objeto actual
  }

  public function funcion_heredada(){
    echo 'Funcion de la clase POSTRE que es heredada a las hijas';
  }

  function get_nombre(){
    return $this->nombre;
  }
}

$flan = new Postre("Flan Napolitano");//Instanciamos un objeto de la clase postre con "new"
                                      //PAsamos como parametro flannapolitano que el CONSTRUCTOR
                                      //recibira como $nombre
                                      //el constuctor lo asigna a su propiedad nombre

echo $flan->get_nombre();

//---------------------------------------------------------------------------------
//METODO CONSTRUCTOR
echo '<br><br>METODO DESTRUCTOR<br>';

class Carro {
  public $modelo;
  public $marca;

  function __construct($modelo) {
    $this->modelo = $modelo;
  }
  function __destruct() {
    echo "<br><br><br>Este es el resultado de __destruct() en la linea 105: El carro es {$this->modelo}.";
  }
}

$carro_audi = new Carro("A1"); //se ejecuta al final del script linea

//-------------------------------------------------------------------------------
//MODIFICADORES DE ACCESO
echo '<br><br>MODIFICADORES DE ACCESO<br>';

class Celular {
  public $nombre;
  protected $bateria;
  private $diagonal_pantalla;
}

$iphone = new Celular();
$iphone->nombre = "XS";

echo 'descomentar la linea 130 y 136 para ver el error';

//$iphone->diagonal_pantalla = 5.9; - se comento para que se ejecute el resto del script

//Fatal error: Uncaught Error: Cannot access private property Celular::$diagonal_pantalla
//in C:\xampp\htdocs\php\scripts\septimoscript.php:123 Stack trace: #0 {main} thrown
//in C:\xampp\htdocs\php\scripts\septimoscript.php

//$iphone->bateria = 4000;- se comento para que se ejecute el resto del script
//Fatal error: Uncaught Error: Cannot access protected property Celular::$bateria
//in C:\xampp\htdocs\php\scripts\septimoscript.php:123 Stack trace: #0 {main} thrown
//in C:\xampp\htdocs\php\scripts\septimoscript.php

//------------------------------------------------------------------------------
echo '<br><br>HERENCIA<br>';

class Pastel extends Postre{ //creamos la clase pastel que extiende de POSTRE
  public function mensaje(){ //FUNCION PROPIA DE LA CLASE HIJA
    echo "<br>Heredo de la clase Postre<br>";
  }
}

$pastel_chocolate = new Pastel("Pastel de chocolate amargo"); //INSTANCIA DE la CLASE PASTEL
$pastel_chocolate->mensaje(); //ACCDEMOS A LA FUNCION MENSAJE DE LA CLASE PASTEL
$pastel_chocolate->funcion_heredada(); //ACCDEMOS A LA FUNCION funcion_heredada de la clase PASTEL
                                      //pero que es heredada de POSTRE.

//------------------------------------------------------------------------------
echo '<br><br>SOBREESCRIBIENDO METODOS HEREDADOS<br>';

class Fruit { //CLASE PADRE FRUIT

  //ATRIBUTOS DE LA CLASE PADRE
  public $name; //ATRIBUTO CLASE PADRE NAME
  public $color; //ATRIBUTO CLASE PADRE COLOR

  //METODOS DE LA CLASE PADRE
  public function __construct($name, $color) { //constructor que recibe 2 parametros
    $this->name = $name;
    $this->color = $color;
  }
  public function intro() { //metodo que describe el nombre y color de la fruta
    echo "The fruit is {$this->name} and the color is {$this->color}.";
  }
}

class Strawberry extends Fruit { //Clase hija que extiende-hereda de Fruit

  //atributos de la clase hija, recordar que ya cuenta con nombre y color
  public $weight;

  //metodos de la clase hija, sobreescritos
  //se llama igual pero recibe 3 parametros, sobrecarga
  public function __construct($name, $color, $weight) {
    $this->name = $name;
    $this->color = $color;
    $this->weight = $weight;
  }

  //se llama igual pero cambia el comprotamiento. sobreescritura
  public function intro() {
    echo "The fruit is {$this->name}, the color is {$this->color}, and the weight is {$this->weight} gram.";
  }
}

$strawberry = new Strawberry("Strawberry", "red", 50); //se instancia la clase hija
$strawberry->intro();//se accede al metodo intro sobreescrito de la clase hija

//--------------------------------------------------------------------------
echo '<br><br>CONSTANTES<br>';

class Despedida {
  const MENSAJE_DESPEDIDA = "Ya nos Vamos"; //nombre en mayusculas
  public function despedirse() {
    echo self::MENSAJE_DESPEDIDA;//se usa :: por que la const sera vista desde fuera de la clase
  }
}

$despedida = new Despedida();//instanciamos el objeto
$despedida->despedirse();
echo '<br>Accediendo a la cosnt desde fuera '.Despedida::MENSAJE_DESPEDIDA;

//----------------------------------------------------------------------------
echo '<br><br>INTERFACES<br>';

interface AnimalInterface { //interface que contendra los metedos a implementar
    public function makeSound(); //el metodo no tiene implementacion, codigo, solo esta definido
}

class Dog implements AnimalInterface {//la clase perro implementa la interfaz
    public function makeSound() { //metodo que perro tiene que implementar
        echo "Implemantacion del metodo makesound por el perro: Woof! <br />";//implementacion del metodo
    }
}
class Cat implements AnimalInterface {//la clase gato implementa la itnerfaz
    public function makeSound() {//metodo que la clase gato tiene que implementar
        echo "Implemantacion del metodo makesound por el gato: Meow! <br />";//implementacion del metodo
    }
}

$perro = new Dog();
$perro->makeSound();

$gato = new Cat();
$gato->makeSound();

//--------------------------------------------------------------------------
echo '<br><br>ABSTRACTAS<br>';

abstract class Fruit2 { //CLASE ABSTRACTA
  private $color; //solo tiene un atributo privado

  abstract public function eat(); //funcion abstracta eat, no tiene implementacion

  public function setColor($c) { //funcion con implementacion que sea el atributo
    $this->color = $c;
  }
}

class Apple2 extends Fruit2 { //apple2 hereda de la clasa abstracta fruit2
  public function eat() { //implementa TODOS sus metodos abstractos, en este caso, 1
    echo "Omnomnom";
  }
}

$manzana2 = new Apple2(); //Instanciamos el objeto de la clase apple2
$manzana2->eat();//accedemos a su metodo implementado que heredo de la clase abstracta

//--------------------------------------------------------------------------------
echo '<br><br>STATIC<br>';

class myClass { //nueva clase
  static $myProperty = 42; //atributo STATIC

  static function myMethod() {//metodo static
    echo self::$myProperty;
  }
}

//accedemos al atriuto y al metodo sin inicializar un objeto
myClass::myMethod();
echo '<br>'.myClass::$myProperty;


?>
