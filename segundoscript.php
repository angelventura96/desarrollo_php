<?php

	//VARIABLES
echo '<br>VARIABLES<br>';

$mi_primer_variable = "Este texto esta en mi primera variable PHP<br>";
//empieza con mayuscula, y tiene guiones bajos.
echo $mi_primer_variable;

/*Las varables son CASE-Sensitive, es decir son sensibles a mayusculas y minusculas
*/
$MI_PRIMER_VARIABLE = "Probando CASE-Sensitive de variables<br>";

echo $MI_PRIMER_VARIABLE;

//No necesitamos definir el tipo de dato de las variables.
$nombre = "Angel";
$edad = 24;

echo "texto: la edad es: " . " ". $edad;

//-----------------------------------------------------------------------------
	//CONSTANTES
echo '<br>';
echo '<br>CONSTANTES<br>';

define("MSG", "<br>Hola desde una constante!");
echo MSG;
   // Outputs "Hola desde una constante!"

define("MSG", "<br>Hola desde una constante no sensible!", true);
 echo msg;

  // Outputs "Hola desde una constante no sensible!!"

//-------------------------------------------------------------------------

//ALCANCE
echo '<br>';
echo '<br>ALCANCE<br>';

function prueba(){
	$local="local";
	echo "variable dentro de la función: $local";
}

prueba();

echo "variable fuera de la función: $local";
//Notice: Undefined variable: local in C:\xampp\htdocs\php\scripts\segundoscript.php on line 43


//------------------------------------------------------------------------
$var_externa = "externa";

function prueba2(){
	echo "variable externa dentro de la función: $var_externa";
	//Notice: Undefined variable: var_externa in C:\xampp\htdocs\php\scripts\segundoscript.php on line 49
}

prueba2();

echo "variable externa fuera de la función: $var_externa";

//------------------------------------------------------------------------
//PALABRA RESERVADA GLOBAL
echo '<br>';
echo '<br>GLOBAL<br>';

$global = 'global';
$global2 = 2;
$global3 = 3;

function prueba3(){

	global $global; //utilizamos la palabra global para poder ver la variable externa
	$notglobal = 'noglobal';
	$global = $global.$notglobal;
	echo '<br>';
	echo $global;
}

function prueba4(){
	$GLOBALS['global2'] = $GLOBALS['global2'] + $GLOBALS['global3'];
}

prueba3();

prueba4();
echo '<br>impresion prueba4<br>';
echo $global2;

//------------------------------------------------------------------------
//STATIC
echo '<br>';
echo '<br> STATIC <br>';

function pruebaStatic() {
  static $x = 0;
  echo 'dentro dela funcion'.$x;
  $x++;
}

pruebaStatic(); //output 0
echo "<br>";
pruebaStatic(); //output 1
echo "<br>";
pruebaStatic(); //output2

//-------------------------------------------------------------------------
 // TIPOS DE DATOS
echo '<br>';
echo '<br>TIPOS DE DATOS<br>';

 //STRING
echo '<br>STRING<br>';
$string1 = "<br>----Hello world!"; //double quotes
$string2 = 'Hello world!';  //single quotes

echo $string1.'<br>';
echo $string2.'<br>';
var_dump($string2);
echo '<br>';

//---------------------------------------------------------------------------
 //FUNCIONES STRRING
echo '<br>FUNCIONES STRING<br>';
//String length
echo strlen("Hello world!").'<br>'; // muestra 12
//string word count
echo str_word_count("Hello world!").'<br>'; // muestra 2
//string position
echo strpos("Quiero saber la posicion donde inicia saber", "saber").'<br>'; // salida 7
//string replace
echo str_replace("world", "Dolly", "Hello world!").'<br>'; // outputs Hello Dolly!

 //---------------------------------------------------------------------------

//Integer
echo '<br>INTEGER<br>';
$int1 = 42; // positive number
$int2 = -42; // negative number

echo "<br>".$int1.'<br>';
echo 'usando var_dump'.var_dump($int2);
echo'<br>';
echo 'usando is_int '.var_dump(is_int($int1));
echo'<br>';
echo 'usando is_integer '.var_dump(is_integer($int2));
echo'<br>';
echo 'usando is_long '.var_dump(is_long($int2));

//----------------------------------------------------------------------------
//FLOAT
echo '<br><br>FLOAT<br>';
$float = 42.168;

echo "<br>".$float;
echo 'usando var_dump'.var_dump($float);
echo'<br>';
echo 'usando is_float '.var_dump(is_float($float));
echo'<br>';
echo 'usando is_double '.var_dump(is_double($int2));

//---------------------------------------------------------------------------
//Numerical strings
echo '<br><br>NUMERICAL STRINGS<br>';
$numstr = 5985;
var_dump(is_numeric($numstr));
echo'<br>';

$numstr2 = "5985";
var_dump(is_numeric($numstr2));
echo'<br>';

$numstr3 = "59.85" + 100;
var_dump(is_numeric($numstr3));
echo'<br>';

$numstr4 = "Hello";
var_dump(is_numeric($numstr4));
echo'<br>';
//---------------------------------------------------------------------
//CAST FLOAT AND STRING TO INT
echo '<br><br>CAST FLOAT AND STRING TO INT<br>';

//Cast float to int
$float_to_cast = 23465.768;
$int_cast1 = (int)$float_to_cast;
echo $int_cast1;

echo "<br>";

// Cast string to int
$string_to_cast = "23465.768";
$int_cast2 = (int)$string_to_cast;
echo $int_cast2;

//---------------------------------------------------------------------
//NUMBER FUNCTIONS
echo '<br><br>Number functions<br>';

//funcion pi
echo '<br>PI<br>';

echo(pi()); // regresa 3.1415926535898
echo "<br>";

//funcion min and max()
echo '<br><br>MIN Y MAX<br>';
echo(min(0, 150, 30, 20, -8, -200));  // regresa -200
echo "<br>";

echo(max(0, 150, 30, 20, -8, -200));  // regresa 150
echo "<br>";

//funcion valor absoluto
echo '<br><br>Valor absoluto<br>';
echo(abs(-6.7));  // regresa en positivo 6.7
echo "<br>";

//funcion raiz cuadrada
echo '<br><br>Valor absoluto<br>';
echo(sqrt(64));  // regresa 8
echo "<br>";

//funcion redondear
echo '<br><br>Round<br>';
echo(round(0.60));  // regresa 1
echo "<br>";

echo(round(0.49));  // regresa 0
echo "<br>";

//funcion random
echo '<br><br>Random<br>';
echo(rand()); //numero aleatorio totalmente
echo "<br>";

echo(rand(10, 100)); //numero entre 10 y 100 inclusive
echo "<br>";

//--------------------------------------------------------------------

//BOOLEAN
echo '<br><br>BOOLEAN<br>';
$true = true;
$false = false;

echo "<br>".$true;
echo '<br>';
//devolvera 1, si fuera la variable $false
echo var_dump($false);

//-------------------------------------------------------------------------

//NULL
echo '<br><br>NULL<br>';
echo '<br>';

$stringnotnull = "Hello world!";
$null = null;


var_dump($stringnotnull);
echo '<br>';
var_dump($null);

//--------------------------------------------------------------------------

$str = "10";
  $int = 20;
  $sum = $str + $int;
  echo "<br><br>Resultado de la suma de un string".$str." y un integer ".$int." es igual a: ".$sum.'<br>';

 //---------------------------------------------------------------------------



?>
