<?php
//VARIABLES SUPERGLOBALES
//$GLOBALS
echo '<br>VARIABLE $GLOBALS<br><br>';

$x = 75; //Variable global
$y = 25;

echo 'TIPO DE DATO DE $GLOBALS:<br> ';
var_dump($GLOBALS);

function addition() {
  $GLOBALS['z'] = $GLOBALS['x'] + $GLOBALS['y'];
  echo '<br><br>';
}

addition();
echo 'El valor de z es: '.$z; //z se definiio como global por eso podemos verla fuera de la funcion addition()

//--------------------------------------------------------------------------------

//$_SERVER
echo '<br><br>VARIABLE $_SERVER<br><br>';

echo 'PHP_SELF: '.$_SERVER['PHP_SELF'];
echo "<br>";
echo 'SERVER_NAME: '.$_SERVER['SERVER_NAME'];
echo "<br>";
echo 'HTTP_POST: '.$_SERVER['HTTP_HOST'];
echo "<br>";
echo 'HTTP_USER_AGENT: '.$_SERVER['HTTP_USER_AGENT'];
echo "<br>";
echo 'SCRIPT_NAME: '.$_SERVER['SCRIPT_NAME'];

//----------------------------------------------------------------------------------

//$_REQUEST
echo '<br><br>VARIABLE $_REQUEST<br><br>';

echo '
<form method="post" action=""?>
  Nombre: <input type="text" name="nombre_formulario">
  <input type="submit">
</form>
';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $nombre_formulario = htmlspecialchars($_REQUEST["nombre_formulario"]);
    if (empty($nombre_formulario)) {
        echo "El campo nombre esta vacio";
    } else {
        echo $nombre_formulario;
    }
}

//-----------------------------------------------------------------------------------
//$POST
echo '<br><br>VARIABLE $:POST<br><br>';

echo '
<form method="post" action=""?>
  Nombre: <input type="text" name="nombre_formulario">
  <input type="submit">
</form>
';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $nombre_formulario = $_POST['nombre_formulario'];
    if (empty($nombre_formulario)) {
        echo "El campo nombre esta vacio";
    } else {
        echo $nombre_formulario;
    }
}

//-----------------------------------------------------------------------------------
//$GET
echo '<br><br>VARIABLE $_GET<br><br>';

echo '<a href="sextoscript.php?subject=PHP&web=BecaEveris">Test $GET</a>';

//hay dos parametros subject con valor PHP y web con valor BecaEveris

//Utilzamos la variable $_GET para recoger los valores de los parametros del link
echo "<br>Estudiando " . $_GET['subject'] . " en " . $_GET['web'];


//------------------------------------------------------------------------------------
//MANEJO DE OFRMULARIOS
echo '<br><br>MANEJO DE FORMULARIOS<br><br>';

echo '
<form action="" method="post">
Nombre Formulario: <input type="text" name="nombref"><br>
E-mail: <input type="text" name="emailf"><br>
<input type="submit">
</form>
';

$nombre = $_POST["nombref"];
$email = $_POST["emailf"];

echo 'Nombre: '.$nombre.'<br>Correo: '.$email;

?>
